package com.booba;

import com.booba.util.XADiskFactory;
import org.assertj.core.api.Assertions;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;
import org.glassfish.jersey.server.ResourceConfig;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.junit.runners.WeldJUnit4Runner;
import org.xadisk.bridge.proxies.interfaces.Session;
import org.xadisk.bridge.proxies.interfaces.XAFileOutputStream;
import org.xadisk.bridge.proxies.interfaces.XAFileSystem;
import org.xadisk.filesystem.exceptions.*;

import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * Created by koita on 22/05/2016.
 */
@RunWith(WeldJUnit4Runner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FileServiceTest {

    @Inject
    private XADiskFactory xaDiskFactory;
    @Inject
    private Server server;
    private WebTarget target;
    private Weld weld;

    @Before
    public void setUp() throws Exception {
        // start the server
//        weld = new Weld();
//        WeldContainer weldContainer = weld.initialize();
//        Server server = weldContainer.instance().select(Server.class).get();
        server.start();
        // create the client
        Client c = ClientBuilder.newBuilder().register(MultiPartFeature.class).build();
        // Create JAX-RS application.
        final Application application = new ResourceConfig()
                .packages("com.booba")
                .register(MultiPartFeature.class);

        // uncomment the following line if you want to enable
        // support for JSON in the client (you also have to uncomment
        // dependency on jersey-media-json module in pom.xml and Server.start())
        // --
        // c.configuration().enable(new org.glassfish.jersey.media.json.JsonJaxbFeature());

        target = c.target(Server.BASE_URI);

    }

    @After
    public void tearDown() throws Exception {
        server.shutdown();
    }

    @Test
    public void test_1_uploadAttachment() throws Exception {
        File file = new File("target/test-classes/Reactive_Microservices_Architecture.pdf");
        InputStream fileInStream = new FileInputStream(file);
        String contentDisposition = "attachment; filename=\"" + file.getName() + "\"";
        // MediaType of the body part will be derived from the file.
        final FileDataBodyPart filePart = new org.glassfish.jersey.media.multipart.file.FileDataBodyPart("file", file);
        final MultiPart multipart = new FormDataMultiPart().field("fileName", file.getName()).field("destPath", "target/test-classes/Reactive_Microservices_Architecture.pdf").bodyPart(filePart);
        Response response = target.path("file").path("upload").request()
                .post(Entity.entity(multipart, multipart.getMediaType()));
        Assertions.assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }

    @Test
    public void test_2_download() throws Exception {
        XAFileSystem xaFileSystem = xaDiskFactory.getXASession();
        Session xaSession = xaFileSystem.createSessionForLocalTransaction();
        File file = new File(new File("target"), "Reactive_Microservices_Architecture-download.pdf");
        if (xaSession.fileExists(file)) {
            xaSession.deleteFile(file);
        }
        xaSession.createFile(file, false);
        System.out.println(file.getAbsoluteFile().getAbsolutePath());
        XAFileOutputStream xaFileOutputStream = xaSession.createXAFileOutputStream(file, true);

        InputStream response = target.path("file").path("download").path("target/test-classes//Reactive_Microservices_Architecture.pdf.zip").request().get(InputStream.class);

        byte[] buffer = new byte[10000];
        int n;
        while ((n = response.read(buffer)) != -1) {
            xaFileOutputStream.write(buffer, 0, n);
        }
        response.close();
        xaFileOutputStream.flush();
        xaFileOutputStream.close();
        xaSession.commit();
        xaFileSystem.shutdown();
    }


}
