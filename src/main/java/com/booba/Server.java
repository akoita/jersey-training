package com.booba;

import com.booba.util.FilePropertiesFile;
import org.apache.deltaspike.core.api.config.ConfigProperty;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.IOException;
import java.net.URI;

/**
 * Server class.
 */
@ApplicationScoped
public class Server {
    // Base URI the Grizzly HTTP server will listen on
    public static final String BASE_URI = "http://localhost:8080/myapp/";

    @Inject
    @ConfigProperty(name = "host")
    private String host;
    @Inject
    @ConfigProperty(name = "port")
    private String port;
    @Inject
    @ConfigProperty(name = "server.name")
    private String serverName;


    private HttpServer httpServer;

    /**
     * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
     *
     * @return Grizzly HTTP server.
     */
    public void start() {

        // create a resource config that scans for JAX-RS resources and providers
        // in com.booba package
        final ResourceConfig rc = new ResourceConfig().packages("com.booba");
        rc.register(MultiPartFeature.class);

        // create and start a new instance of grizzly http server
        // exposing the Jersey application at BASE_URI
        String baseUrl = String.format("http://%s:%s/%s/", host, port, serverName);
        httpServer = GrizzlyHttpServerFactory.createHttpServer(URI.create(baseUrl), rc);
    }

    public void shutdown() {
        httpServer.shutdown();
    }

    /**
     * Server method.
     *
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        Weld weld = new Weld();
        WeldContainer weldContainer = weld.initialize();
        Server server = weldContainer.instance().select(Server.class).get();
        server.start();
        System.out.println(String.format("Jersey app started with WADL available at "
                + "%sapplication.wadl\nHit enter to stop it...", BASE_URI));
        server.shutdown();
        weld.shutdown();
        System.in.read();
    }
}

