package com.booba.util;

import org.xadisk.bridge.proxies.interfaces.Session;
import org.xadisk.bridge.proxies.interfaces.XAFileSystem;
import org.xadisk.bridge.proxies.interfaces.XAFileSystemProxy;
import org.xadisk.filesystem.standalone.StandaloneFileSystemConfiguration;

import java.io.File;

/**
 * Created by koita on 22/05/2016.
 */
public class XADiskFactory {
    private static int cpt = 0;

    public XAFileSystem getXASession() throws InterruptedException {
        String suffix = cpt++ + "-" + System.currentTimeMillis();
        String xadiskSystemDirectory = "C:\\xadisk\\" + suffix;
        File sampleDataDir1 = new File("C:\\data1");
        XAFileSystem xafs = null;
        StandaloneFileSystemConfiguration configuration =
                new StandaloneFileSystemConfiguration(xadiskSystemDirectory, "id-" + suffix);
        configuration.setTransactionTimeout(240);

        xafs = XAFileSystemProxy.bootNativeXAFileSystem(configuration);

        System.out.println("\nBooting XADisk...\n");

        xafs.waitForBootup(-1);

        System.out.println("\nXADisk is now available for use.\n");

        return xafs;
    }
}
