package com.booba.util;

import org.apache.deltaspike.core.api.config.PropertyFileConfig;

public class FilePropertiesFile implements PropertyFileConfig {

    private static final long serialVersionUID = 1L;

    @Override
    public String getPropertyFileName() {
        return "file.properties";
    }

    @Override
    public boolean isOptional() {
        return false;
    }

}
