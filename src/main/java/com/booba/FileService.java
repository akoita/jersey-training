package com.booba;

import com.booba.util.XADiskFactory;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.xadisk.bridge.proxies.interfaces.Session;
import org.xadisk.bridge.proxies.interfaces.XAFileOutputStream;
import org.xadisk.bridge.proxies.interfaces.XAFileSystem;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

@Path("file")
public class FileService {

    @Inject
    private XADiskFactory xaDiskFactory;
    private File f;

    @POST
    @Path("/upload/")
//    @Consumes(MediaType.APPLICATION_OCTET_STREAM)
//    public void uploadAttachment(@PathParam("attachmentName") String attachmentName, InputStream attachmentInputStream) {
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response postForm(FormDataMultiPart formDataMultiPart) {
        try {
            FormDataBodyPart filePart = formDataMultiPart.getField("file");
            InputStream attachmentInputStream = filePart.getValueAs(InputStream.class);

            FormDataBodyPart fileNamePart = formDataMultiPart.getField("fileName");
            String fileNamePartValue = fileNamePart.getValue();

            FormDataBodyPart destPathPart = formDataMultiPart.getField("destPath");
            String destPathPartValue = destPathPart.getValue();


            byte[] buffer = new byte[1000];
            int n = -1;

            XAFileSystem xaFileSystem = xaDiskFactory.getXASession();
            Session session = xaFileSystem.createSessionForLocalTransaction();
            f = new File(new File(destPathPartValue).getParent(), fileNamePartValue + ".zip");
            if (f.exists()) {
                session.deleteFile(f);
            }
            session.createFile(f, false);
            XAFileOutputStream xaFileOutputStream = session.createXAFileOutputStream(f, true);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ZipOutputStream zipOutputStream = new ZipOutputStream(byteArrayOutputStream);
            ZipEntry zipEntry = new ZipEntry(fileNamePartValue);
            zipOutputStream.putNextEntry(zipEntry);
            while ((n = attachmentInputStream.read(buffer)) != -1) {
                zipOutputStream.write(buffer, 0, n);
//                xaFileOutputStream.write(buffer, 0, n);
            }
            zipOutputStream.closeEntry();
            zipOutputStream.close();
            xaFileOutputStream.write(byteArrayOutputStream.toByteArray());
            xaFileOutputStream.flush();
            xaFileOutputStream.close();
            session.commit();
            xaFileSystem.shutdown();
        } catch (Throwable e) {
            e.printStackTrace();
            return Response.serverError().build();
        }
        return Response.ok().build();
    }


    @GET
    @Path("/download/{filePath:.+}")
    public Response download(@PathParam("filePath") final String filePath) {
        StreamingOutput streamingOutput = new StreamingOutput() {
            @Override
            public void write(OutputStream output) throws IOException, WebApplicationException {
                ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(filePath));
                ZipEntry nextEntry = zipInputStream.getNextEntry();
                byte[] buffer = new byte[10000];
                int n;
                while ((n = zipInputStream.read(buffer)) != -1) {
                    output.write(buffer, 0, n);
                }
                zipInputStream.closeEntry();
                zipInputStream.close();
            }
        };

        return Response
                .ok(streamingOutput, MediaType.APPLICATION_OCTET_STREAM)
//                .header("content-disposition","attachment; filename = myfile.pdf")
                .build();
    }


    @DELETE
    @Path("/delete/{fileName}")
    public Response delete(@PathParam("fileName") final String fileName) {
//        Session xaSession = getXASession();
//        xaSession.mo
        return null;
    }


}
